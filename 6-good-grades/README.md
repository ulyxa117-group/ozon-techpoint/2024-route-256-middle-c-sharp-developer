## 6. Хорошие отметки (30 баллов)
#### Ограничения на тест: 3 000 Мс / 256 Мб
#### :ballot_box_with_check: Тесты пройдены
## Условие задачи
В школе Игоря действует 5-балльная система оценки. Чем больше число, тем лучше считается оценка: `5` означает наилучшую оценку, а `1` самую плохую.

Успеваемость Игоря записана в дневнике в виде таблицы из $n$ строк и $m$ столбцов, где в каждой клетке записана цифра от `1` до `5`.<br>
Игорь очень переживает, что за плохие оценки его будут ругать родители. Он придумал, как он может избавиться от плохих оценок в дневнике: убрать одну строку и один столбец из дневника так, чтобы самая плохая оставшаяся оценка в таблице была как можно лучше.
## Входные данные
Каждый тест состоит из нескольких наборов входных данных.

Первая строка содержит целое число $t\space(1\le t\le 2\cdot 10^5)$ — количество наборов входных данных.

Далее следует описание наборов входных данных.

Первая строка каждого набора входных данных содержит целые числа $n$ и $m\space(2\le n,m\le 5\cdot 10^5,2\le n\cdot m\le 10^6)$.

В каждой из следующих $n$ строк содержатся по $m$ цифр от `1` до `5` без пробелов — таблица оценок Игоря.

Гарантируется, что сумма $n\cdot m$ по всем наборам входных данных не превосходит $10^6$.
## Выходные данные
Выведите номер строки и номер столбца, от которых нужно избавиться Игорю, чтобы наихудшая оценка в таблице стала как можно лучше. Если подходящих ответов несколько, выведите любой из них.

Во втором примере мы можем убрать четвертую строку и второй столбец, так что останутся оценки:
```
554
354
545
553
```
Таким образом худшей оценкой будет `3`.
## Пример теста 1
### Входные данные
```
2
2 2
53
32
5 4
5254
3454
5545
4514
5253
```
### Выходные данные
```
2 2
4 2
```