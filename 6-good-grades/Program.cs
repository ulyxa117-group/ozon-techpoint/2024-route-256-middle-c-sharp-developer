﻿//using var input = new StreamReader(File.OpenRead(@"..\input.txt"));
using var input = new StreamReader(Console.OpenStandardInput());
using var output = new StreamWriter(Console.OpenStandardOutput());

var t = int.Parse(input.ReadLine()!);
while (t > 0)
{
    t--;
    DoWork(input, output);
}
return;

static void DoWork(TextReader input, StreamWriter output)
{
    var s = input.ReadLine()!.Split();
    var n = int.Parse(s[0]);
    var m = int.Parse(s[1]);

    var a = new string[n];
    for (var i = 0; i < n; i++)
    {
        a[i] = input.ReadLine()!;
    }

    var (rows, columns, _) = CalculateSummary(a, n, m);

    var rowForDeletion1 = FindOptimalIndexForDeletion(rows);
    var aNew = DeleteRow(a, rowForDeletion1);
    var (newRows, newColumns, _) = CalculateSummary(aNew, n - 1, m);
    var columnForDeletion1 = FindOptimalIndexForDeletion(newColumns);
    aNew = DeleteColumn(aNew, columnForDeletion1);
    var (_, _, min1) = CalculateSummary(aNew, n - 1, m - 1);

    var columnForDeletion2 = FindOptimalIndexForDeletion(columns);
    aNew = DeleteColumn(a, columnForDeletion2);
    (newRows, newColumns, _) = CalculateSummary(aNew, n, m - 1);
    var rowForDeletion2 = FindOptimalIndexForDeletion(newRows);
    aNew = DeleteRow(aNew, rowForDeletion2);
    var (_, _, min2) = CalculateSummary(aNew, n - 1, m - 1);

    var (column, row) = min1 > min2 ? (columnForDeletion1, rowForDeletion1) : (columnForDeletion2, rowForDeletion2);
    output.WriteLine($"{row + 1} {column + 1}");
}

static (int[][] rows, int[][] columns, int minValue) CalculateSummary(string[] a, int n, int m)
{
    var minValue = 5;

    var rows = new int[n][];
    var columns = new int[m][];
    for (var i = 0; i < n; i++)
    {
        rows[i] = new int[5];
        for (var j = 0; j < m; j++)
        {
            if (i == 0)
            {
                columns[j] = new int[5];
            }

            var value = a[i][j] - '1';
            rows[i][value]++;
            columns[j][value]++;

            if (value < minValue) minValue = value;
        }
    }

    return (rows, columns, minValue);
}

static int FindOptimalIndexForDeletion(int[][] collection)
{
    var index = -1;
    return collection
        .Select(cell =>
        {
            index++;
            return new
            {
                Index = index,
                Cell = cell,
            };
        })
        .OrderByDescending(x => x.Cell[0])
        .ThenByDescending(x => x.Cell[1])
        .ThenByDescending(x => x.Cell[2])
        .ThenByDescending(x => x.Cell[3])
        .ThenByDescending(x => x.Cell[4])
        .First()
        .Index;
}

static string[] DeleteRow(string[] a, int index)
{
    var result = new string[a.Length - 1];
    var delta = 0;
    for (var i = 0; i < a.Length; i++)
    {
        if (i == index)
        {
            delta = -1;
            continue;
        }
        result[i + delta] = a[i];
    }

    return result;
}

static string[] DeleteColumn(string[] a, int index)
{
    var result = new string[a.Length];
    for (var i = 0; i < a.Length; i++)
    {
        result[i] = a[i].Remove(index, 1);
    }

    return result;
}