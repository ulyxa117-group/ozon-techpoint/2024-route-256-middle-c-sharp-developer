﻿//using var input = new StreamReader(File.OpenRead(@"..\input.txt"));
using var input = new StreamReader(Console.OpenStandardInput());
using var output = new StreamWriter(Console.OpenStandardOutput());

var t = int.Parse(input.ReadLine()!);
while (t > 0)
{
    t--;
    DoWork(input, output);
}
return;

static void DoWork(TextReader input, StreamWriter output)
{
    Point a = null;
    Point b = null;

    var s = input.ReadLine()!.Split();
    var n = int.Parse(s[0]);
    var m = int.Parse(s[1]);
    for (var y = 0; y < n; y++)
    {
        var line = input.ReadLine()!;
        for (var x = 0; x < m; x++)
        {
            switch (line[x])
            {
                case 'A':
                    a = new Point(x, y);
                    break;
                case 'B':
                    b = new Point(x, y);
                    break;
            }
        }
    }

    FindPaths(output, n, m, a!, b!);
}

static void FindPaths(StreamWriter output, int n, int m, Point a, Point b)
{
    var aLetter = 'a';
    var bLetter = 'b';
    if (a.X >= b.X && a.Y >= b.Y)
    {
        (a, b) = (b, a);
        (aLetter, bLetter) = (bLetter, aLetter);
    }

    var aPath = FindPathToZero(a);
    var bPath = FindTransposedPathToZero(b, n, m);

    var aPos = GetClosestPos(aPath, m);
    var bPos = GetClosestPos(bPath, m);

    for (var y = 0; y < n; y++)
    {
        for (var x = 0; x < m; x++)
        {
            if (x % 2 == 1 && y % 2 == 1)
            {
                output.Write("#");
            }
            else if (IsSamePoint(a, x, y))
            {
                output.Write(aLetter.ToString().ToUpperInvariant());
            }
            else if (IsSamePoint(b, x, y))
            {
                output.Write(bLetter.ToString().ToUpperInvariant());
            }
            else if (IsSamePoint(aPos.Current, x, y))
            {
                aPos.MoveNext();
                output.Write(aLetter);
            }
            else if (IsSamePoint(bPos.Current, x, y))
            {
                bPos.MoveNext();
                output.Write(bLetter);
            }
            else
            {
                output.Write(".");
            }
        }

        output.WriteLine();
    }
}

static IEnumerable<Point> FindPathToZero(Point a)
{
    while (true)
    {
        if (a is { X: 0, Y: 0 }) yield break;

        var next = a.X == 0 || a.Y % 2 == 1
            ? new Point(a.X, a.Y - 1)
            : new Point(a.X - 1, a.Y);

        yield return next;
        a = next;
    }
}

static IEnumerable<Point> FindTransposedPathToZero(Point a, int n, int m)
    => FindPathToZero(Transpose(a, n, m)).Select(p => Transpose(p, n, m));

static IEnumerator<Point> GetClosestPos(IEnumerable<Point> path, int m)
{
    var @enum = path.OrderBy(p => p.Y * m + p.X).GetEnumerator();
    @enum.MoveNext();
    return @enum;
}

static Point Transpose(Point a, int n, int m) => new(m - a.X - 1, n - a.Y - 1);

static bool IsSamePoint(Point a, int x, int y) => a != null && a.X == x && a.Y == y;

internal class Point
{
    public int X { get; }
    public int Y { get; }

    public Point(int x, int y)
    {
        X = x;
        Y = y;
    }
}