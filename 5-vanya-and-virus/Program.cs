﻿using System.Text;
using System.Text.Json;

//using var input = new StreamReader(File.OpenRead(@"..\input.txt"));
using var input = new StreamReader(Console.OpenStandardInput());
using var output = new StreamWriter(Console.OpenStandardOutput());

var t = int.Parse(input.ReadLine()!);
while (t > 0)
{
    t--;
    DoWork(input, output);
}
return;

static void DoWork(TextReader input, StreamWriter output)
{
    var n = int.Parse(input.ReadLine()!);
    var sb = new StringBuilder();
    while (n > 0)
    {
        n--;
        sb.Append(input.ReadLine()!);
    }

    var dir = JsonSerializer.Deserialize<Directory>(sb.ToString(), new JsonSerializerOptions
    {
        MaxDepth = 810,
        PropertyNameCaseInsensitive = true,
    })!;
    var hackedFilesCount = CalculateHackedFiles(dir, isHacked: false);
    output.WriteLine(hackedFilesCount);
}

static int CalculateHackedFiles(Directory dir, bool isHacked)
{
    if (!isHacked)
    {
        isHacked = IsHacked(dir);
    }

    var curCount = 0;
    if (isHacked)
    {
        curCount = dir.Files?.Count() ?? 0;
    }

    if (dir.Folders != null)
    {
        curCount += dir.Folders.Select(f => CalculateHackedFiles(f, isHacked)).Sum();
    }

    return curCount;
}

static bool IsHacked(Directory dir) =>
    dir.Files?.Any(f => f.EndsWith(".hack", StringComparison.InvariantCultureIgnoreCase)) ?? false;

internal class Directory
{
    public string Dir { get; set; }
    public List<string>? Files { get; set; }
    public List<Directory>? Folders { get; set; }
}