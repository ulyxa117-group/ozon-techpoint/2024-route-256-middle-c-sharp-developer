﻿//using var input = new StreamReader(File.OpenRead(@"..\input.txt"));
using var input = new StreamReader(Console.OpenStandardInput());
using var output = new StreamWriter(Console.OpenStandardOutput());

var t = int.Parse(input.ReadLine()!);
while (t > 0)
{
    t--;

    decimal result = 0;
    var s = input.ReadLine()!.Split();
    var n = int.Parse(s[0]);
    var p = int.Parse(s[1]);

    while (n > 0)
    {
        n--;

        var a = int.Parse(input.ReadLine()!);

        var correct = a / 100m * p;
        result += correct % 1;
    }

    output.WriteLine(result.ToString("0.00"));
}