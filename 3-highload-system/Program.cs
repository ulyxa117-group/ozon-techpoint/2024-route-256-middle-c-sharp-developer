﻿//using var input = new StreamReader(File.OpenRead(@"..\input.txt"));
using var input = new StreamReader(Console.OpenStandardInput());
using var output = new StreamWriter(Console.OpenStandardOutput());

var map = new Dictionary<char, int>
{
    { 'M', 0 },
    { 'R', 1 },
    { 'C', 2 },
    { 'D', 3 },
};

var rules = new[]
{   //       M  R  C  D
    new [] { 0, 1, 1, 1 }, // M
    new [] { 0, 0, 1, 0 }, // R
    new [] { 1, 0, 0, 0 }, // C
    new [] { 1, 0, 0, 0 }, // D
};

var t = int.Parse(input.ReadLine()!);
while (t > 0)
{
    t--;

    var s = input.ReadLine()!;

    var currentState = 3;
    foreach (var step in s.Select(c => map[c]))
    {
        if (rules[currentState][step] == 0)
        {
            currentState = -1;
            break;
        }

        currentState = step;
    }

    output.WriteLine(currentState == 3 ? "YES" : "NO");
}