﻿//using var input = new StreamReader(File.OpenRead(@"..\input.txt"));
using var input = new StreamReader(Console.OpenStandardInput());
using var output = new StreamWriter(Console.OpenStandardOutput());

var n = int.Parse(input.ReadLine()!);
var logins = new HashSet<string>();
for (var i = 0; i < n; i++)
{
    var login = input.ReadLine()!;
    foreach (var l in GetLoginMasks(login))
    {
        logins.Add(l);
    }
}

var m = int.Parse(input.ReadLine()!);
for (var i = 0; i < m; i++)
{
    var login = input.ReadLine()!;
    output.WriteLine(logins.Contains(login) ? "1" : "0");
}

return;

static IEnumerable<string> GetLoginMasks(string login)
{
    yield return login;
    for (var j = 0; j < login.Length - 1; j++)
    {
        var l = login[..j] + login[j + 1] + login[j] + login[(j + 2)..];
        yield return l;
    }
}