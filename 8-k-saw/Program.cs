﻿//using var input = new StreamReader(File.OpenRead(@"..\input.txt"));
using var input = new StreamReader(Console.OpenStandardInput());
using var output = new StreamWriter(Console.OpenStandardOutput());

var t = int.Parse(input.ReadLine()!);

while (t > 0)
{
    t--;
    DoWork(input, output);
}

return;

static void DoWork(TextReader input, StreamWriter output)
{
    var n = int.Parse(input.ReadLine()!);
    var map = GetMapOfSequences(input);
    var result = CalculateResult(map, n);
    foreach (var i in result) output.Write($"{i} ");
    output.WriteLine();
}

static List<int> GetMapOfSequences(TextReader input)
{
    var s = input.ReadLine()!.Split();

    var lastElement = int.Parse(s[0]);
    var curSeqLength = 0;
    var isGrowingSeq = true;
    var result = new List<int>();

    for (var i = 1; i < s.Length + 1; i++)
    {
        var curElement = i < s.Length ? int.Parse(s[i]) : lastElement;

        if (curElement > lastElement && isGrowingSeq || curElement < lastElement && !isGrowingSeq)
        {
            curSeqLength++;
        }
        else
        {
            if (curSeqLength > 0 || result.LastOrDefault() != 0)
            {
                result.Add(isGrowingSeq ? curSeqLength : -curSeqLength);
            }

            if (curElement == lastElement)
            {
                result.Add(0);
                curSeqLength = 0;
            }
            else
            {
                curSeqLength = 1;
                isGrowingSeq = curElement > lastElement;
            }
        }

        lastElement = curElement;
    }

    return result;
}

static int[] CalculateResult(List<int> map, int n)
{
    var maxResult = new int[n];
    var curResult = new int[n];
    var maxSeqLen = 0;

    for (var i = 0; i < map.Count + 1; i++)
    {
        var curElement = i < map.Count ? map[i] : 0;
        var absCurElement = Math.Abs(curElement);

        for (var k = absCurElement; k < maxSeqLen; k++)
        {
            if (curResult[k] == 0) continue;
            BrakeSeq(maxResult, curResult, k);
        }

        for (var k = 0; k < absCurElement; k++)
        {
            if (curResult[k] > 0)
            {
                if (absCurElement == k + 1)
                {
                    curResult[k]++;
                }
                else if (absCurElement > k + 1)
                {
                    curResult[k]++;
                    BrakeSeq(maxResult, curResult, k);
                }
                else
                {
                    BrakeSeq(maxResult, curResult, k);
                }
            }

            if (curResult[k] == 0 && curElement > 0)
            {
                curResult[k]++;
            }

            if (curResult[k] > 0 && k >= maxSeqLen) maxSeqLen = k + 1;
        }
    }

    return maxResult;
}

static void BrakeSeq(int[] max, int[] cur, int k)
{
    var curLen = cur[k] / 2;
    if (curLen > max[k])
    {
        max[k] = curLen;
    }

    cur[k] = 0;
}