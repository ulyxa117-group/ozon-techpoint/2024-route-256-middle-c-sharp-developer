﻿//using var input = new StreamReader(File.OpenRead(@"..\input.txt"));
using var input = new StreamReader(Console.OpenStandardInput());
using var output = new StreamWriter(Console.OpenStandardOutput());

var s = input.ReadLine()!.Split();
var a = int.Parse(s[0]);
var b = int.Parse(s[1]);
output.Write(a - b);