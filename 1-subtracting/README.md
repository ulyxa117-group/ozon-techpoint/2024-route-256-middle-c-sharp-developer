## 1. Разность чисел (5 баллов)
#### Ограничения на тест: 1 000 Мс / 256 Мб
#### :ballot_box_with_check: Тесты пройдены

## Условие задачи
Прежде чем вы приступите к решению задач, используйте эту, чтобы познакомиться с платформой Techpoint, а также получить первые 5 баллов. Считайте из стандартного потока ввода два целых числа и выведите в стандартный поток вывода их разность.
## Входные данные
Единственная строка входных данных содержит два целых числа $a$ и $b\space(−10^9\le a,b\le 10^9)$.
## Выходные данные
Выведите значение выражения $a−b$.
## Пример теста 1
### Входные данные
```
2 -2
```
### Выходные данные
```
4
```
## Пример теста 2
### Входные данные
```
4 2
```
### Выходные данные
```
2
```
## Пример теста 3
### Входные данные
```
1 1000000000
```
### Выходные данные
```
-999999999
```