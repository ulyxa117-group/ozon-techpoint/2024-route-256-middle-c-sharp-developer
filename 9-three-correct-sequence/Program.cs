﻿//using var input = new StreamReader(File.OpenRead(@"..\input.txt"));
using var input = new StreamReader(Console.OpenStandardInput());
using var output = new StreamWriter(Console.OpenStandardOutput());

var t = int.Parse(input.ReadLine()!);
while (t > 0)
{
    t--;
    DoWork(input, output);
}

return;

static void DoWork(TextReader input, StreamWriter output)
{
    input.ReadLine();
    var message = input.ReadLine()!;
    var wrongWays = new HashSet<string>();
    output.WriteLine(IsCorrect(message, 0, 0, 0, wrongWays) ? "Yes" : "No");
}

static bool IsCorrect(string message, int index, int xCount, int yCount, HashSet<string> wrongWays)
{
    var key = $"{index}_{xCount}_{yCount}";
    if (wrongWays.Contains(key))
    {
        return false;
    }

    var result = false;
    if (index == message.Length)
    {
        result = xCount == 0 && yCount == 0;
    }
    else if (xCount + yCount > message.Length - index)
    {
        result = false;
    }
    else
    {
        switch (message[index])
        {
            case 'X':
                result = IsCorrect(message, index + 1, xCount + 1, yCount, wrongWays);
                break;
            case 'Y':
                result = IsCorrect(message, index + 1, xCount, yCount + 1, wrongWays) ||
                         xCount > 0 && IsCorrect(message, index + 1, xCount - 1, yCount, wrongWays);
                break;
            case 'Z':
                if (xCount + yCount == 0) result = false;
                else
                    result = yCount > 0 && IsCorrect(message, index + 1, xCount, yCount - 1, wrongWays) ||
                             xCount > 0 && IsCorrect(message, index + 1, xCount - 1, yCount, wrongWays);
                break;
        }
    }

    if (!result)
    {
        wrongWays.Add(key);
    }

    return result;
}